﻿using EnvDTE;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace VSGitExtensions
{
    public class GitChangesVM : INotifyPropertyChanged
    {
        private Lazy<EnvDTE.DTE> dteLazy = new Lazy<EnvDTE.DTE>(() => (EnvDTE.DTE)Microsoft.VisualStudio.Shell.Package.GetGlobalService(typeof(EnvDTE.DTE)));
        private int SecondsToRefresh = 30;
        private int SecondsToRefreshWhenSolutionClosed = 5;


        public event PropertyChangedEventHandler PropertyChanged;
        private DispatcherTimer timer = new DispatcherTimer();
        private List<string> m_FilesChanged;
        private string GitRootLocation;

        public List<string> FilesChanged
        {
            get
            {
                return m_FilesChanged;
            }
            set
            {
                m_FilesChanged = value;
                PropChanged(nameof(FilesChanged));
            }
        }

        private string m_SelectedBranch;

        public string SelectedBranch
        {
            get { return m_SelectedBranch; }
            set
            {
                m_SelectedBranch = value;
                PropChanged(nameof(SelectedBranch));
                Refresh();
            }
        }

        private string[] m_BranchNames;

        public string[] BranchNames
        {
            get { return m_BranchNames; }
            set
            {
                m_BranchNames = value;
                PropChanged(nameof(BranchNames));
            }
        }

        private string m_CurrBranch;

        public string CurrBranch
        {
            get { return m_CurrBranch; }
            set
            {
                m_CurrBranch = value;
                PropChanged(nameof(CurrBranch));
            }
        }


        private ObservableCollection<TreeItemVM> m_FilesChangedTree;

        public ObservableCollection<TreeItemVM> FilesChangedTree
        {
            get { return m_FilesChangedTree; }
            set
            {
                m_FilesChangedTree = value;
                PropChanged(nameof(FilesChangedTree));
            }
        }
        private Dictionary<string, ProjectItemVM> projectsFolders = new Dictionary<string, ProjectItemVM>(StringComparer.OrdinalIgnoreCase);

        private HashSet<string> usedFiles = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
        private string lastSolutionFilePath = "";

        public GitChangesVM()
        {
            // Start local folder listener.
            timer.Interval = TimeSpan.FromSeconds(SecondsToRefreshWhenSolutionClosed);
            timer.Start();
            timer.Tick += Timer_Tick;
            FilesChanged = new List<string>();

            BranchNames = new string[0];
            SelectedBranch = null;

        }

        private string GetCurrSolutionFolder()
        {
            string solutionDir = string.Empty;
            EnvDTE.DTE dte = dteLazy.Value;

            if (!string.IsNullOrEmpty(dte.Solution.FullName))
            {
                solutionDir = System.IO.Path.GetDirectoryName(dte.Solution.FullName);
            }
            return solutionDir;
        }

        private IEnumerable<Tuple<string, string>> GetProjects()
        {
            return GetProjects(null);
        }


        //private readonly Lazy<IVsSolutionWorkspaceService> _solutionWorkspaceService = new Lazy<IVsSolutionWorkspaceService>(
        //() => (IVsSolutionWorkspaceService)ServiceProvider.GetService(typeof(SVsSolutionWorkspaceService)));



        ///// <summary>
        ///// Get the active solution file.
        ///// </summary>
        ///// <returns>The solution file.</returns>
        //public string GetActiveSolutionFile()
        //    {
        //        return _solutionWorkspaceService.Value.SolutionFile;
        //    }

        //    /// <summary>
        //    /// Get the projects in a solution.
        //    /// </summary>
        //    /// <param name="solutionFilePath">the solution file path.</param>
        //    /// <returns></returns>
        //    public async Task<IEnumerable<string>> GetProjectsInSolutionAsync(string solutionFilePath)
        //    {
        //        var workspace = _solutionWorkspaceService.Value.CurrentWorkspace;
        //        var indexService = workspace.GetIndexWorkspaceService();

        //        // Passing the configuration|Platform that projects apply to; passing null will return projects with all configurations.
        //        var result = await indexService.GetFileReferencesAsync(solutionFilePath, context: "Debug|AnyCPU", referenceTypes: (int)FileReferenceInfoType.ProjectReference);
        //        return result.Select(f => f.Path);
        //    }

        private IEnumerable<Tuple<string, string>> GetProjects(IEnumerable<Project> projects)
        {
            string solutionDir = string.Empty;
            EnvDTE.DTE dte = dteLazy.Value;

            if (!string.IsNullOrEmpty(dte.Solution.FullName))
            {
                if (projects == null)
                {
                    projects = ToProjectsList(dte.Solution.Projects);
                }

                //solutionDir = System.IO.Path.GetDirectoryName(dte.Solution.FullName);
                foreach (EnvDTE.Project proj in projects)
                {
#if VS2015
                    var projKind = "{66A26720-8FB5-11D2-AA7E-00C04F688DDE}";
#else
                    var projKind = EnvDTE80.ProjectKinds.vsProjectKindSolutionFolder;
#endif
                    if (proj != null)
                    {
                        if (proj.Kind == projKind)
                        {
                            var projs = GetProjects(ToProjectsList(proj.ProjectItems));
                            foreach (var p in projs)
                            {
                                yield return p;
                            }
                        }
                        else
                        {
                            bool success = false;
                            var fullName = string.Empty;
                            try
                            {
                                if (proj.FullName != "")
                                {
                                    var fileInfo = new FileInfo(proj.FullName);
                                    fullName = fileInfo.Directory.FullName;
                                    success = true;
                                }
                            }
                            catch (Exception ex)
                            {

                            }

                            if (success)
                            {
                                yield return new Tuple<string, string>(proj.Name, fullName);
                            }
                        }
                    }
                }
            }
            //return solutionDir;
        }

        private IEnumerable<Project> ToProjectsList(Projects projects)
        {
            foreach (EnvDTE.Project proj in projects)
            {
                yield return proj;
            }
        }

        private IEnumerable<Project> ToProjectsList(ProjectItems projectFolder)
        {
            foreach (ProjectItem projItem in projectFolder)
            {
                yield return projItem.SubProject;
            }
        }

        private string gitDiffCache = string.Empty;
        private string gitBranchCache = string.Empty;
        private string lastBranchUsedToCompare = string.Empty;
        private void Timer_Tick(object sender, EventArgs e)
        {
            Refresh();
        }


        public async void Refresh(bool force = false)
        {
            try
            {
                if (!timer.IsEnabled)
                {
                    return;
                }

                timer.Stop();
                timer.IsEnabled = false;

                var currSolution = GetCurrSolutionFolder();

                if (string.IsNullOrEmpty(currSolution))
                {
                    timer.Interval = TimeSpan.FromSeconds(SecondsToRefreshWhenSolutionClosed);
                }
                else
                {
                    timer.Interval = TimeSpan.FromSeconds(SecondsToRefresh);
                }

                if (currSolution != lastSolutionFilePath || lastBranchUsedToCompare != this.SelectedBranch)
                {
                    lastBranchUsedToCompare = this.SelectedBranch;
                    lastSolutionFilePath = currSolution;
                    usedFiles = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                    FilesChangedTree = new ObservableCollection<TreeItemVM>();
                    gitDiffCache = string.Empty;
                    gitBranchCache = string.Empty;
                    GitRootLocation = string.Empty;
                    projectsFolders = new Dictionary<string, ProjectItemVM>(StringComparer.OrdinalIgnoreCase);
                    folderForFiles = new Dictionary<string, FolderItemVM>(StringComparer.OrdinalIgnoreCase);

                    //FilesChangedTree = new ObservableCollection<TreeItemVM>(new List<TreeItemVM>()
                    //{
                    //    new ProjectItemVM()
                    //    {
                    //        Name = "test",
                    //        Items = new ObservableCollection<TreeItemVM>(new List<TreeItemVM>()
                    //        {
                    //            new FolderItemVM()
                    //            {
                    //                Name="test folder",
                    //                Items=new ObservableCollection<TreeItemVM>(new List<TreeItemVM>(){
                    //                    new FileItemVM()
                    //                    {
                    //                        Name="Test File",

                    //                    }
                    //                })
                    //            }
                    //        })
                    //    }
                    //});
                }

                OutputWindowHelper.Output("Getting branch names");
                var branchNamesString = await RunCommand("branch");
                if (force || branchNamesString != gitBranchCache)
                {
                    gitBranchCache = branchNamesString;
                    var tempBranchNames = branchNamesString.Split('\n');
                    var currBranchMatchs = Regex.Match(branchNamesString, "\\* *([\\w|/|_|\\-|0-9]*)");

                    if (currBranchMatchs.Groups.Count > 1)
                    {
                        this.CurrBranch = currBranchMatchs.Groups[1].Value;
                    }

                    this.BranchNames = tempBranchNames.Select(name => Regex.Match(name, "\\** *([\\w|/|_|\\-|0-9]*)")?.Groups[1]?.Value).Where(name => !string.IsNullOrEmpty(name)).ToArray();

                    if (string.IsNullOrEmpty(SelectedBranch) && this.BranchNames.Length > 0)
                    {
                        if (this.BranchNames.Contains("master"))
                        {
                            SelectedBranch = "master";
                        }
                        else
                        {
                            SelectedBranch = this.BranchNames[0];
                        }
                    }
                }

                OutputWindowHelper.Output("Getting git data");
                var filesRes = await RunCommand($"diff --name-only {SelectedBranch}");
                var filesResLocal = await RunCommand($"diff --name-only");
                filesRes += "\r\n" + filesResLocal;

                if (force || gitDiffCache != filesRes)
                {
                    gitDiffCache = filesRes;
                    this.FilesChanged = new List<string>(filesRes.Split('\n').Distinct());

                    if (force || string.IsNullOrEmpty(GitRootLocation))
                    {
                        GitRootLocation = (await RunCommand($"rev-parse --show-toplevel")).Replace("\n", "").Replace("/", "\\");
                    }

                    var projectNames = GetProjects().ToList();

                    ObservableCollection<TreeItemVM> temp = this.FilesChangedTree;
                    if (temp == null)
                    {
                        temp = new ObservableCollection<TreeItemVM>();
                    }

                    foreach (var file in this.FilesChanged)
                    {
                        if (!usedFiles.Contains(file) && !string.IsNullOrEmpty(file))
                        {
                            // Tryied to handle non unicode names
                            //Regex.Replace(fileForPath, "\\\\([0-9]{3,6})", (match) =>
                            // {
                            //     var num = match.Groups[1]?.Value;
                            //     var res = match.Groups[0].Value;
                            //     if (string.IsNullOrEmpty(num))
                            //     {
                            //         int val = int.Parse(num);

                            //     }

                            //     return "";
                            // });

                            try
                            {
                                var filePath = System.IO.Path.Combine(GitRootLocation, file.Replace("/", "\\"));

                                var originalProj = projectNames.Where(proj => filePath.StartsWith(proj.Item2)).FirstOrDefault();

                                if (originalProj == null)
                                {
                                    originalProj = new Tuple<string, string>("* Empty Project *", "EmptyProject://");
                                }

                                if (originalProj != null)
                                {
                                    usedFiles.Add(file);

                                    ProjectItemVM projectFolder = null;
                                    if (projectsFolders.ContainsKey(originalProj.Item2))
                                    {
                                        projectFolder = projectsFolders[originalProj.Item2];
                                    }
                                    else
                                    {
                                        projectFolder = new ProjectItemVM()
                                        {
                                            Name = originalProj.Item1,
                                            ProjectPath = originalProj.Item2
                                        };
                                        projectsFolders[originalProj.Item2] = projectFolder;
                                        temp.Add(projectFolder);
                                    }

                                    if (!folderForFiles.ContainsKey(file))
                                    {
                                        AddFileIntoFolder(projectFolder, file, filePath, originalProj.Item2);
                                    }
                                }
                            }
                            catch
                            {
                                OutputWindowHelper.Output("There is a file name that cannot be resolved : " + file);
                                usedFiles.Add(file);
                            }
                        }
                    }

                    this.FilesChanged.ToDictionary(i => i);

                    List<KeyValuePair<string, FolderItemVM>> filesToRemove = new List<KeyValuePair<string, FolderItemVM>>();
                    // Remove files that are not in the list of changed files.
                    foreach (var fileInFolder in folderForFiles)
                    {
                        if (!this.FilesChanged.Contains(fileInFolder.Key))
                        {
                            filesToRemove.Add(fileInFolder);
                            
                            //fileInFolder.Value.ItemsDic.Remove(fileName);
                            //fileInFolder.Value.Items.Remove(fileToRemove);
                        }
                    }

                    foreach (var fileInFolder in filesToRemove)
                    {
                        var fileName = System.IO.Path.GetFileName(fileInFolder.Key);
                        var fileToRemove = fileInFolder.Value.ItemsDic[fileName];
                        RemoveFile(fileToRemove);
                    }

                    if (this.FilesChangedTree != temp)
                    {
                        this.FilesChangedTree = temp;
                    }
                }
            }
            catch (Exception ex)
            {
                OutputWindowHelper.Output("Failed to get git diff info \r\n" + ex.ToString());
            }
            finally
            {
                timer.IsEnabled = true;
                timer.Start();
            }
        }

        private void RemoveFile(TreeItemVM item)
        {
            if (item.Parent != null)
            {
                var folder = item.Parent;
                item.Parent = null;
                folder.ItemsDic.Remove(item.Name);
                folder.Items.Remove(item);

                if (folder.Items.Count == 0)
                {
                    RemoveFile(folder);
                }
            }
            else
            {
                var proj = item as ProjectItemVM;
                if (proj != null)
                {
                    if (projectsFolders.ContainsKey(proj.ProjectPath))
                    {
                        projectsFolders.Remove(proj.ProjectPath);
                    }
                }
            }

            var file = item as FileItemVM;
            if (file != null)
            {
                if (folderForFiles.ContainsKey(file.RelativePath))
                {
                    usedFiles.Remove(file.RelativePath);
                    folderForFiles.Remove(file.RelativePath);
                }
            }
        }

        private Dictionary<string, FolderItemVM> folderForFiles = new Dictionary<string, FolderItemVM>(StringComparer.OrdinalIgnoreCase);

        private void AddFileIntoFolder(FolderItemVM folder, string file, string fileFullPath, string projectRoot)
        {
            FileInfo info = new FileInfo(fileFullPath);
            List<string> folderNames = new List<string>();
            var dir = info.Directory;
            while (dir.FullName != projectRoot && dir.FullName.Length > projectRoot.Length)
            {
                folderNames.Insert(0, dir.Name);
                dir = dir.Parent;
            }

            FolderItemVM currDir = folder;
            for (int i = 0; i < folderNames.Count; i++)
            {
                var folderName = folderNames[i];
                if (currDir.ItemsDic.ContainsKey(folderName))
                {
                    currDir = currDir.ItemsDic[folderName] as FolderItemVM;
                }
                else
                {
                    var tempNewFolder = new FolderItemVM()
                    {
                        Name = folderName,
                        Parent = currDir
                    };

                    currDir.ItemsDic[folderName] = tempNewFolder;
                    currDir.Items.Add(tempNewFolder);
                    currDir = tempNewFolder;
                }
            }

            if (currDir != null)
            {
                var fileItem = new FileItemVM()
                {
                    Parent = currDir,
                    Name = info.Name,
                    RelativePath = file,
                    FullName = fileFullPath
                };

                currDir.ItemsDic[info.Name] = fileItem;
                currDir.Items.Add(fileItem);
                folderForFiles[file] = currDir;
            }
        }

        public async void DiffFile(FileItemVM file)
        {
            // not working on branches with private/yosal/somthing

            var tempFileContent = await RunCommand($"show \"{this.SelectedBranch}\":\"{file.RelativePath}\"");

            var tempFolder = System.IO.Path.Combine(System.IO.Path.GetTempPath(), "VSGitExtensions");
            if (!System.IO.Directory.Exists(tempFolder))
            {
                System.IO.Directory.CreateDirectory(tempFolder);
            }

            var tempFilePath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), "VSGitExtensions\\" + Guid.NewGuid() +"_" + file.Name);
            System.IO.File.WriteAllText(tempFilePath, tempFileContent);

            dteLazy.Value.ExecuteCommand("Tools.DiffFiles",
                string.Format("\"{0}\" \"{1}\" \"{2}\" \"{3}\"", tempFilePath, file.FullName,
                $"{this.SelectedBranch}: {file.RelativePath}", $"{this.CurrBranch}: {file.RelativePath}"));


            //dteLazy.Value.ItemOperations.dif

            //await RunCommand(Environment.ExpandEnvironmentVariables(@"%VS110COMNTOOLS%\..\IDE\CommonExtensions\Microsoft\TeamFoundation\Team Explorer\vsDiffMerge.exe"), 
            //                 $" \"{tempFilePath}\" \"{file.FullName}\" /t");
            //await RunCommand($"difftool --no-prompt \"{this.SelectedBranch}\" \"{file.FullName}\"");
            //git show master: HeshbonotClient / site / site / mobile / default.aspx
            //C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\Common7\IDE\CommonExtensions\Microsoft\TeamFoundation\Team Explorer\vsDiffMerge.exe
            // %VS110COMNTOOLS%\..\IDE
            //  /t  - open on previouse tab
        }

        public void OpenFile(FileItemVM file)
        {
            //await RunCommand($"difftool --no-prompt {this.CurrBranch} \"{file.FullName}\"");
            //dteLazy.Value.IsOpenFile(System.IO.Path.Combine(file.FullName));
            try
            {

                dteLazy.Value.ItemOperations.OpenFile(file.FullName);
            }
            catch (Exception ex)
            {
                OutputWindowHelper.Output("Failed to open file: \n" + ex.ToString());
            }
        }

        private async Task<string> RunCommand(string gitCommand)
        {
            return await RunCommand("git", gitCommand);
        }

        private async Task<string> RunCommand(string exePath, string gitCommand)
        {
            string result = string.Empty;

            await Task.Run(() =>
            {
                string path = string.Empty;

                var testPath = lastSolutionFilePath;
                if (!string.IsNullOrEmpty(testPath))
                {
                    path = testPath;
                }

                var proc = System.Diagnostics.Process.Start(new ProcessStartInfo()
                {
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    StandardOutputEncoding = Encoding.UTF8,
                    //RedirectStandardInput = true,
                    FileName = exePath,
                    Arguments = "-C \"" + path + "\" --no-pager " + gitCommand,
                    WorkingDirectory = path
                });

                OutputWindowHelper.Output("\r\nRunning Command:  " + gitCommand);

                //int i = 20;
                //while (!proc.HasExited)
                //{
                //    proc.StandardInput.WriteLine();
                //    proc.StandardInput.WriteLine();
                //    proc.StandardInput.WriteLine();
                //    proc.StandardInput.WriteLine();
                //    System.Threading.Thread.Sleep(10);
                //    i--;
                //    if (i < 0)
                //    {
                //        break;
                //    }
                //}

                var gitInfo = proc.StandardOutput.ReadToEnd();

                if (proc.WaitForExit(5 * 1000))
                {
                    OutputWindowHelper.Output($"command result {gitCommand}  \r\n==============\r\n" + gitInfo);
                    result = gitInfo;
                }
                else
                {
                    OutputWindowHelper.Output($"command {gitCommand} timmed out.");
                }

                if (!proc.HasExited)
                {
                    proc.Kill();
                }
            });

            return result;
        }

        public void PropChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }

    public class TreeItemVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public FolderItemVM Parent { get; set; }

        public string Name { get; set; }

        public void PropChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }

    public class FolderItemVM : TreeItemVM
    {
        public Dictionary<string, TreeItemVM> ItemsDic { get; set; } = new Dictionary<string, TreeItemVM>(StringComparer.OrdinalIgnoreCase);
        private ObservableCollection<TreeItemVM> m_Items;

        public ObservableCollection<TreeItemVM> Items
        {
            get { return m_Items; }
            set
            {
                m_Items = value;
                PropChanged(nameof(Items));
            }
        }

        public FolderItemVM()
        {
            Items = new ObservableCollection<TreeItemVM>();
        }
    }

    public class ProjectItemVM : FolderItemVM
    {
        public string ProjectPath { get; set; }
    }

    public class FileItemVM : TreeItemVM
    {
        public string RelativePath { get; set; }
        public string FullName { get; set; }
    }
}
