﻿using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSGitExtensions
{
    public class OutputWindowHelper
    {
        //private static bool WasPaneCreated = false;
        private static Guid PanGuid = Guid.NewGuid();
        private static IVsOutputWindowPane generalPane = null;

        public static void Output(string msg)
        {
            //System.Windows.MessageBox.Show("");
            IVsOutputWindow outWindow = Package.GetGlobalService(typeof(SVsOutputWindow)) as IVsOutputWindow;

            //Guid generalPaneGuid =VSConstants.GUID_OutWindowGeneralPane; // P.S. There's also the GUID_OutWindowDebugPane available.

            if (generalPane == null)
            {
                //if (!WasPaneCreated)
                //{
                outWindow.CreatePane(ref PanGuid, "Git branch names diffs output window", 1, 1);
                outWindow.GetPane(ref PanGuid, out generalPane);
                generalPane.Activate(); // Brings this pane into view            
                //WasPaneCreated = true;
                //}
            }
            //else if()
            //{
            //    outWindow.GetPane(ref PanGuid, out generalPane);
            //}

            generalPane.OutputString(msg);
        }
    }
}
