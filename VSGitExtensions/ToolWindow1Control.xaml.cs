﻿//------------------------------------------------------------------------------
// <copyright file="ToolWindow1Control.xaml.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace VSGitExtensions
{
    using System.Diagnostics.CodeAnalysis;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;

    /// <summary>
    /// Interaction logic for ToolWindow1Control.
    /// </summary>
    public partial class ToolWindow1Control : UserControl
    {
        private GitChangesVM vm = new GitChangesVM();
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolWindow1Control"/> class.
        /// </summary>
        public ToolWindow1Control()
        {
            this.InitializeComponent();
            this.DataContext = vm;
        }

        /// <summary>
        /// Handles click on the button by displaying a message box.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
        [SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions", Justification = "Sample code")]
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Default event handler naming pattern")]
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show(
            //    string.Format(System.Globalization.CultureInfo.CurrentUICulture, "Invoked '{0}'", this.ToString()),
            //    "Files change in this branch");
        }

        private void TreeView_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var t = sender as TreeView;

            if (t.SelectedItem is FileItemVM)
            {
                vm.DiffFile(t.SelectedItem as FileItemVM);
            }
        }

        private void TreeView_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var t = sender as FrameworkElement;
            //var item = t.DataContext as FileItemVM;

            if (t != null)
            {
                TreeViewItem tvi = null;
                for (var i = 0; i < 20; i++)
                {
                    var p = VisualTreeHelper.GetParent(t);
                    tvi = p as TreeViewItem;
                    t = p as FrameworkElement;

                    if (t ==null || tvi != null)
                    {
                        break;
                    }
                }

                if (tvi != null)
                {
                    tvi.IsSelected = true;
                }
            }
            //if (t.DataContext is FileItemVM)
            //{
            //    //var item = Tree.ItemContainerGenerator.ContainerFromItem(t.DataContext as FileItemVM) as TreeViewItem;
            //    //vm.OpenFile(t.DataContext as FileItemVM);
            //    item.IsSelected = true;
            //}
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            vm.Refresh(true);
        }

        private void MenuItemOpen_Click(object sender, RoutedEventArgs e)
        {
            var t = sender as Control;

            if (t.DataContext is FileItemVM)
            {
                vm.OpenFile(t.DataContext as FileItemVM);
            }
        }

        private void MenuItemCompare_Click(object sender, RoutedEventArgs e)
        {
            var t = sender as Control;

            if (t.DataContext is FileItemVM)
            {
                vm.DiffFile(t.DataContext as FileItemVM);
            }
        }
    }
}